function(add_app name)
	# Get all the C++ files composing the epuck-app application
	file(GLOB files ${CMAKE_CURRENT_SOURCE_DIR}/${name}/*.cpp)

	# Create an executable using these files
	add_executable(${name} ${files})

	# Enable C++11
	target_compile_features(${name} PRIVATE cxx_std_11)

	# Add OpenCV include directory
	target_include_directories(${name} PRIVATE ${OpenCV_INCLUDE_DIRS})

	# Link with OpenCV and pthread libraries
	target_link_libraries(${name} ${OpenCV_LIBS} ${CMAKE_THREAD_LIBS_INIT} epuck)
endfunction()

add_app(epuck-app)
add_app(imgProcessing-app)
